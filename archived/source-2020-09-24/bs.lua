--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭、陈夏等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)、LLCOM
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com，如有侵权嫌疑将立即纠正
---------------------------------------------------------------------------------------------------------------------------------------
--- 模块功能：核心板适配文件，适配硬件版本Cat1 Phone Core V2b
-- @author miuser
-- @module midemo.bs
-- @license MIT
-- @copyright miuser@luat
-- @release 2020-09-05
--------------------------------------------------------------------------
-- @说明部分
--------------------------------------------------------------------------

-- 引脚		名称		功能
-- B1		GND		    电源负极
-- B2		4V IN		锂电池供电输入
-- B3		5V IN		5V直流电源输入
-- B4		UART1_TX	串口1发送 
-- B5		UART1_RX	串口1接收 
-- B6		GPIO19		双向输入输出IO
-- B7		GPIO18		双向输入输出IO
-- B8		RESET		复位
-- B9		POWER ON	电源按键
-- B10		UART2_TX	串口2发送
-- B11		UART2_RX	串口2接收
-- B12		GPIO13		不建议使用
-- B13		1.8V OUT	1.8V供电输出
-- B14		MIC+		麦克风正极
-- B15		MIC-		麦克风负极
-- B16		SPK-		喇叭负极
-- B17		SPK+		喇叭正极
-- A1		GND		    电源负极
-- A2		SCL		    I2C总线时钟
-- A3		SDA		    I2C总线数据
-- A4		SPI_CS		SPI总线片选
-- A5		SPI_CLK		SPI总线时钟
-- A6		SPI_MOSI	SPI总线数据输出
-- A7		SPI_MISO	SPI总线数据输入
-- A8		GND		    显示屏供电负极
-- A9		VLCD		显示屏供电正极
-- A10		LCD_CK		显示屏时钟
-- A11		LCD_DA		显示屏数据
-- A12		LCD_RST		显示屏复位
-- A13		LCD_DC		显示屏命令数据切换
-- A14		LCD_CS		显示屏片选
-- A15		ADC3		模拟输入3
-- A16		ADC2		模拟输入2
-- A17		GND		    电源负极
-- C1		GND		    电源负极
-- C2		D-		    USB差分数据负极
-- C3		D+		    USB差分数据正极
-- C4		UART3_TX	串口3发送
-- C5		UART3_RX	串口3接收
-- C6		5V IN		5V直流电源输入

--------------------------------------------------------------------------
-- @用户可修改部分
--------------------------------------------------------------------------
----------------------引脚分配表-----------------------------
------------------------------------------------------------
------------------------------------------------------------
require "lbsLoc"
require "misc"
require "nvm"
module(...,package.seeall)
-- 将串口1，2，3及USB虚拟串口分配作为命令控制接口
COM_UART_IDs={1,2,3,129}  
--GPIO13,18，19映射为双向IO端口
BIOPins={13,18,19}

------------------------------------------------------------


------------------------------------------------------------
----------------------引脚映射表-----------------------------
------------------------------------------------------------
MAP={}
--核心板的B6引脚映射为GPIO19
MAP["B6"]="19"
--核心板的B7引脚映射为GPIO18
MAP["B7"]="18"
--核心板的B12引脚映射为GPIO13，这个引脚为内置LED使用
MAP["B12"]="13"
------------------------------------------------------------

--------------------核心板状态-------------------------------
--isLocated：number类型，0表示成功，1表示网络环境尚未就绪，2表示连接服务器失败，3表示发送数据失败，4表示接收服务器应答超时，5表示服务器返回查询失败；为0时，后面的3个参数才有意义
--LATITUDE：string类型，纬度，整数部分3位，小数部分7位，例如031.2425864
--LONGITUDE：string类型，经度，整数部分3位，小数部分7位，例如121.4736522
LONGITUDE,LATITUDE,isLocated=0,0,0
--本模块的IMEI号
IMEI=""
--本模块的SN
SN=""
--MTJ模块的ID
ID=""
--MTJ模块的MM
MM=""
--------------------------------------------------------------



---------以下内容为内部函数，不需要用户修改--------------------

--查找输入的参数是否存在映射替换
function PIN_MAP(boardpin)
   if (MAP[boardpin]~=nil) then 
        return MAP[boardpin]
   else
        return boardpin
   end
end

function Trim_CMD(cmd)
    for i=1,#cmd do
        if (cmd:sub(#cmd,#cmd)=="\n") or (cmd:sub(#cmd,#cmd)=="\r") then 
            cmd=string.sub(cmd,1,#cmd-1) 
            --log.info("bs","TrimedCMD is "..cmd.." Len is "..#cmd)
        end
    end
    return cmd
end

local function reqLbsLoc()   
     lbsLoc.request(getLocCb)
 end
--获取基站对应的经纬度后的回调函数
function getLocCb(result,lat,lng)
     log.info("testLbsLoc.getLocCb",result,lat,lng)
     isLocated=result
     LATITUDE=lat
     LONGITUDE=lng
 
     --获取经纬度成功
     if result==0 then
     --失败
     else
     end
     sys.timerStart(reqLbsLoc,20000)
 end
 reqLbsLoc()


 -- 加载关机存储模块 （关机需要保存的变量定义在该模块内）
require "NVMPara"
nvm.init("NVMPara.lua")
IMEI=nvm.get("IMEI")
SN=nvm.get("SN")
ID=nvm.get("ID")
MM=nvm.get("MM")
NVMPara.ServerTable=nvm.get("ServerTable")


if (IMEI=="" or SN=="" or ID=="" or MM=="") then 
    sys.timerStart(function() 
        --获取IMEI和SN号
        IMEI=tostring(misc.getImei())
        SN=tostring(misc.getSn())  
        ID=string.sub(IMEI,-10)
        MM="00"..SN
        --保存参数
        nvm.set("ServerTable",{ip="box.miuser.net",port=7101,})
        nvm.set("IMEI",IMEI)
        nvm.set("SN",SN)
        nvm.set("ID",ID)
        nvm.set("MM",MM)

        log.info("boardinfo","restarting to save para")
        rtos.restart()
    end,5000)
end

sys.timerLoopStart(function() 
    log.info("boardinfo","IMEI=",IMEI)
    log.info("boardinfo","SN=",SN)
    log.info("boardinfo","ID=",ID)
    log.info("boardinfo","MM=",MM)
end ,3000 )

