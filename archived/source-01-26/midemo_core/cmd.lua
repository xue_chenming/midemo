--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭、陈夏等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)、LLCOM
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com，如有侵权嫌疑将立即纠正
---------------------------------------------------------------------------------------------------------------------------------------
--- 模块功能：系统指令模块
-- @author miuser
-- @module midemo.cmd
-- @license MIT
-- @copyright miuser@luat
-- @release 2020-09-17
--------------------------------------------------------------------------
-- @说明部分
--------------------------------------------------------------------------

require "lbsLoc"
require "misc"
require "nvm"
module(...,package.seeall)

--标签打印
sys.subscribe("GETID",function(...)
    log.info("label maker","GETID")
    ret="ID="..bs.ID.."\r\n"
    sys.publish("COM",ret)

    sys.publish("NET_CMD_MONI",ret)
end)
sys.subscribe("GETMM",function(...)
    log.info("label maker","GETMM")
    ret="MM="..bs.MM.."\r\n"
    sys.publish("COM",ret)
    sys.publish("NET_CMD_MONI",ret)
end)

