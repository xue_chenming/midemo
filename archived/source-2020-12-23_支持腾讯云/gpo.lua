--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭、陈夏等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)、LLCOM(Apache2)
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com 
---------------------------------------------------------------------------------------------------------------------------------------

-- @模块功能：GPIO引脚输出控制
-- @author miuser
-- @module midemo.gpo
-- @license MIT
-- @copyright miuser@luat
-- @release 2020-10-07
--------------------------------------------------------------------------
-- @使用方法
-- @串口收到形如SETGPO,pio,level的指令后，对应的pio引脚设置为level状态，比如 串口收到SETGPO,13,1，则GPIO13引脚被设置为高电平
-- @当串口收到SETGPIO指令时，串口将收到中文提示 GPIOxx当前电平为高/低的提示
-- @串口收到形如GETGPO,pio的指令后，系统发送主题为GPIO_LEVEL 主题的消息，比如串口收到 GETGPO,13 将发送系统消息GPO_LEVEL,13,1 
-- @消息收发均采用utf8编码，与lua文件系统相同

require"pins"
require"utils"
require"pm"
require"common"

module(...,package.seeall)

--上一次的电平状态表，以管脚号为索引
local pinLastStage={}

--通过消息发送调试信息到串口模块
function write(s)
    --log.info("testUartTask.write",s)
    sys.publish("COM",s)
    sys.publish("NET_CMD_MONI",s)
end

write("Initialing GPO")


sys.subscribe("SETGPO",function(...)

    io=tonumber(arg[1])
    level=tonumber(arg[2])
    mute=tonumber(arg[3])
    --保存对应IO口状态，备查
    pinLastStage[io]=level
    pins.setup(io,level)              
    if (mute~=1) then 
        if (level==0) then write("GPOLEVEL,"..tostring(io)..",0".."\r\n") else write("GPOLEVEL,"..tostring(io)..",1".."\r\n") end 
        sys.publish("GPO_LEVEL",io,level)
    end   
end)

sys.subscribe("GETGPO",function(...)

    io=tonumber(arg[1])
    level=pinLastStage[io]
    if (level==0) then write("GPOLEVEL,"..tostring(io)..",0".."\r\n") else write("GPOLEVEL,"..tostring(io)..",1".."\r\n") end 
    sys.publish("GPIO_LEVEL",io,level)
end)


