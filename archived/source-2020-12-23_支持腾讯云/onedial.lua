--------------------------------------------------------------------------------------------------------------------------------------- 
-- 版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭，等
-- 项目源码重要贡献者：月落无声
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com 
---------------------------------------------------------------------------------------------------------------------------------------

--- 模块功能 一键拨号功能
-- @author miuser
-- @module elderphone.onedial
-- @license MIT
-- @copyright miuser
-- @release 2020-09-24

require"pins"
require"common"
require "mcc"
require "audio"
module(...,package.seeall)

audio.setCallVolume(7)

local okToDial=0
local incoming=0
local onTime,offTime=0,0
--要呼出的号码
local callingnumber=""
local cnumber=""
require "NVMPara"
nvm.init("NVMPara.lua")
callingnumber=nvm.get("CALLNUMBER")

if (callingnumber=="") then
    callingnumber="117"
end

function setcallnumber(num)
    callingnumber=num
    s="CALL NUMBER="..num.."\r\n"
    sys.publish("COM",s)
    sys.publish("NET_CMD_MONI",s)
    log.info("onedial","saving call number is "..num)
    nvm.set("CALLNUMBER",num)
end
sys.subscribe("SETCALLNUMBER", setcallnumber)


--GPIO1，绿灯
local ledr = pins.setup(pio.P0_1,0)
pmd.ldoset(7,pmd.LDO_VLCD)
-- 接收LED参数配置参数
local function mblink(ontime,offtime)
    ledr(1)
    onTime=ontime
    offTime=offtime
    if offTime==0 then
        ledr(0)
    elseif onTime==0 then
        ledr(1)
    end
end

--指令拨号
local function mdial(num)
    if okToDial==1 then
        if (num==nil) then
            cnumber=callingnumber
            num=cnumber
        end
        mcc.dial(num)
        s="DIALING "..num.."\r\n"
        sys.publish("COM",s)
        sys.publish("NET_CMD_MONI",s)
    else
        s="NOT READY TO DIAL ".."\r\n"
        sys.publish("COM",s)
        sys.publish("NET_CMD_MONI",s)
    end
end
sys.subscribe("DIAL",mdial)

function answercall(msg)
    if (incoming==1) then 
        s="ANSWERING "..cnumber.."\r\n"
        
        mcc.accept(cnumber)
        sys.publish("COM",s)
        sys.publish("NET_CMD_MONI",s)

    else
        s="NO CALL TO ANSWER ".."\r\n"
        sys.publish("COM",s)
        sys.publish("NET_CMD_MONI",s)
    end  
end
sys.subscribe("ANSWERCALL", answercall)

function pickup()
    sys.publish("ANSWERCALL")
end
sys.subscribe("PICKUP", pickup)

--硬件按键拨号
function oktodial(msg)
    if (incoming==1) then 
        mcc.accept(cnumber)
    elseif (incoming==0) then
        if okToDial==1 then
            mcc.dial(callingnumber)
            cnumber=callingnumber
        end
    else 
        mcc.hangUp(cnumber)
    end
end

sys.subscribe("ONEDIAL", oktodial)

function hangup(msg)
    log.info("hanging up all call")
    mcc.hangUp(cnumber)
    s="HUNGUP "..cnumber.."\r\n"
    sys.publish("COM",s)
    sys.publish("NET_CMD_MONI",s)
end
sys.subscribe("HUNGUP",hangup)

local function oktodial()
    sys.timerStart(function()
        okToDial=1
        mblink(0,1000)
    end,3000)
end
sys.subscribe("NET_STATE_REGISTERED", oktodial)

local function cincoming(num)
    log.info("onedial","cincoming number is "..num)
    s="INCOMING CALL "..num.."\r\n"
    sys.publish("COM",s)
    sys.publish("NET_CMD_MONI",s)
    cnumber=num
    incoming=1
    mblink(100,100)
    audio.play(CALL,"FILE","/lua/mali.mp3",audiocore.VOL7,nil,true)
end
sys.subscribe("MCALL_INCOMING",cincoming)

local function connected(num)
    cnumber=num
    incoming=2
    mblink(500,500)
end
sys.subscribe("MCALL_CONNECTED",connected)

local function disconnected(num)
    incoming=0
    mblink(0,1000)
    audio.stop()
    cnumber=callingnumber
    s="DISCONNECTED "..cnumber.."\r\n"
    sys.publish("COM",s)
    sys.publish("NET_CMD_MONI",s)
end
sys.subscribe("MCALL_DISCONNECTED",disconnected)






sys.taskInit(function()
    local oncount,offcount=0,0
    local stage="on"
    while true do
        if onTime~=0 and offTime~=0 then 
            if stage=="on" then
                if oncount<(onTime/20) then
                    ledr(0)

                    oncount=oncount+1
                    --log.info("elderphone.light","turn on all three led",oncount)
                else
                    oncount=0
                    stage="off"
                    --log.info("elderphone.light","stage=off")
                end
            end
            if stage=="off" then 
                if offcount<(offTime/20) then
                    ledr(1)         
                    offcount=offcount+1
                    --log.info("elderphone.light","turn off all three led",offcount)
                else
                    offcount=0
                    stage="on"
                    --log.info("elderphone.light","stage=on")
                end
            end
        end
        sys.wait(20)
    end
end
)

