--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭、陈夏等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)、LLCOM(Apache2)
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com 
---------------------------------------------------------------------------------------------------------------------------------------

-- @模块功能：按钮按键模块
-- @author miuser
-- @module midemo.btn
-- @license MIT
-- @copyright miuser@luat
-- @release 2020-10-16
--------------------------------------------------------------------------
-- @使用方法
-- @按钮连接到对应的IO口和1.8V之间
-- @当短按按钮时，将向串口发送BUTTON_SHORT_PRESSED,XX 消息
-- @当长按按钮超过1.5S时，将向串口发送BUTTON_LONG_PRESSED,XX 消息
-- @同时系统还会发送消息 BTN_SHORT_PRESSED 和 BTN_LONG_PRESSED 的消息,消息有一个参数是按钮的GPIO号


require"pins"
require"utils"
require"pm"
require"common"

module(...,package.seeall)


--上一次的电平状态表，以管脚号为索引
 local pinLastStage={}
--获取GPIO状态的函数表，以管脚号为索引
local getGpioFnc={}
--当收到GPIO输入测试的时候执行回调函数

--通过消息发送调试信息到串口模块
function write(s)
    --log.info("testUartTask.write",s)
    sys.publish("COM",s)
    sys.publish("NET_CMD_MONI",s)
end
function gpioIntFnc(msg)
    local trigerPin=""
    local response=""
    --检测哪个IO口发生电平变化
    for i, v in ipairs(bs.BTNPins) do
        if getGpioFnc[v]()~=pinLastStage[v] then
            trigerPin=v
            pinLastStage[v]=getGpioFnc[v]()
        end
    end
    if (trigerPin=="") then return end
    local level=getGpioFnc[trigerPin]()
    --上升沿中断
    if msg==cpu.INT_GPIO_POSEDGE then
        sys.publish("GPIO_LEVEL_CHANGE",trigerPin,0)
    else
     --下降沿中断       
        sys.publish("GPIO_LEVEL_CHANGE",trigerPin,1)
    end
end

for i=1,#bs.BTNPins do
    pinLastStage[bs.BTNPins[i]]=0
end

for i=1,#bs.BTNPins do
    --设置中断函数和电平检测函数
    getGpioFnc[bs.BTNPins[i]]=pins.setup(bs.BTNPins[i],gpioIntFnc)
    --引脚均设为下拉
    pio.pin.setpull(pio.PULLDOWN,bs.BTNPins[i])
    
end

---按钮事件
sta,longcb,shortcb,longtimercb={},{},{},{}

for i=1,#bs.BTNPins do
    sta[i]= "IDLE"
    longtimercb[bs.BTNPins[i]]=function() 
        --log.info("midemo.btn","pin.longtimercb "..bs.BTNPins[i])
        sta[bs.BTNPins[i]] = "LONGPRESSED"
        longcb[bs.BTNPins[i]]()
    end
    shortcb[bs.BTNPins[i]]=function()
        log.info("midemo.btn","pin.shortpress "..bs.BTNPins[i])
        write("BUTTON_SHORT_PRESSED,"..bs.BTNPins[i])
        sys.publish("BTN_SHORT_PRESSED",bs.BTNPins[i])
    end
    longcb[bs.BTNPins[i]]=function()
        log.info("midemo.btn","pin.longpress "..bs.BTNPins[i])
        write("BUTTON_LONG_PRESSED,"..bs.BTNPins[i])
        sys.publish("BTN_LONG_PRESSED",bs.BTNPins[i])
    end
end

--响应对应pin的按钮，edge=0 上升沿， edge=1 下降沿
local function keyMsg(pin,edge)
    if edge==0 then
        sta[pin] = "PRESSED"
        sys.timerStart(longtimercb[pin],1500)
    else
        sys.timerStop(longtimercb[pin])
        if sta[pin]=="PRESSED" then
            if shortcb[pin] then shortcb[pin]() end
		end
		sta[pin] = "IDLE"
	end
end
sys.subscribe("GPIO_LEVEL_CHANGE", keyMsg) 