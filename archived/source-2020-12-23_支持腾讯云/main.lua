
PROJECT = "Mi_Demo"
VERSION = "0.0.1"
AUTHOR = "Miuser"
PRODUCT_KEY = "eTRqO1hu3CpciZVfRdLfPPRu0VVarKRI "

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE
--LOG_LEVEL =log.LOG_SILENT
--[[
如果使用UART输出日志，打开这行注释的代码"--log.openTrace(true,1,115200)"即可，根据自己的需求修改此接口的参数
如果要彻底关闭脚本中的输出日志（包括调用log模块接口和Lua标准print接口输出的日志），执行log.openTrace(false,第二个参数跟调用openTrace接口打开日志的第二个参数相同)，例如：
1、没有调用过sys.opntrace配置日志输出端口或者最后一次是调用log.openTrace(true,nil,921600)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false)即可
2、最后一次是调用log.openTrace(true,1,115200)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false,1)即可
]]
--log.openTrace(true,1,115200)

require "sys"
require "net"
require "utils"
require "patch"
--每8S查询一次GSM信号强度
--每1分钟查询一次基站信息
net.startQueryAll(8000, 60000)

require "errDump"
errDump.request("udp://ota.airm2m.com:9072")
require "ntp"
ntp.timeSync(24, function()log.info(" AutoTimeSync is Done!") end)

--加载控制台调试功能模块（此处代码配置的是uart2，波特率115200）
--此功能模块不是必须的，根据项目需求决定是否加载
--使用时注意：控制台使用的uart不要和其他功能使用的uart冲突
--使用说明参考demo/console下的《console功能使用说明.docx》
--require "console"
--console.setup(2, 115200)

rtos.sleep(3000)
log.info("midemo","midemo starting")
log.info("Free Flash:", math.floor(rtos.get_fs_free_size()/1000).."KB")-- 打印剩余FALSH，单位Byte
--启动并加载核心板适配包
require "bs"
-- 加载帮助模块
require "help"
--加载iRTU适配器
require "irtu_init"
--iRTU核心
require "default"
--加载后清理工作
require "irtu_over"
--加载upws服务连接程序
require "upws"
--加载串口映射模块
require "com"
--根据bs设置，决定是否加载OLED显示屏驱动
if (bs.OLED==true) then require "oled" end
--加载IO输出驱动
require "gpo"
--加载双向IO驱动
require "bio"
--加载命令解释模块
require "cmd"
--加载电源按键模块
require "mkey"
--加载打电话模块
require "onedial"
--打补丁后的语音朗读模块（占用100kflash）
if (bs.ttsplus==true) then require "ttsplus" end
--原生语音朗读模块，与 TTSPLUS二选一加载即可
if (bs.tts==true) then require"tts" end
--SHT30读取模块
require "sht30"
--短信收发
require "trsms"
--根据bs设置，决定是否加载WS2812Matrix显示屏驱动
if (bs.WS2812BMatrix==true) then require "ws2812bmatrix" end
--根据bs设置，决定是否加载Hyperstepper开源电机驱动
if (bs.Hyperstepper==true) then require "hyperstepper" end
--加载GPS模块
if (bs.GPS==true) then require "gpsplus" end
--加载按钮模块
if (bs.Btn==true) then require "btn" end
--加载mqtt模块
if (bs.MQTT==true) then require "mmqtt" end
--加载阿里云模块
if (bs.ALIYUN==true) then require "maliyun" end
--加载腾讯云模块
if (bs.TXIOT==true) then require "mtxiot" end
--加载调试模块
require "demo"
--启动系统框架
sys.init(0, 0)
sys.run()

