--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭、陈夏等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)、LLCOM
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com，如有侵权嫌疑将立即纠正
---------------------------------------------------------------------------------------------------------------------------------------
--- 模块功能：upws服务器适配
-- @author miuser
-- @module midemo.upws
-- @license MIT
-- @copyright miuser@luat
-- @release 2020-09-014
--------------------------------------------------------------------------
-- @说明部分
--------------------------------------------------------------------------
require "net"
require "misc"
require "common"
module(...,package.seeall)

P13Level=0

function gb(str)
    return common.utf8ToGb2312(str)
end

--回报状态信息
function ReportStatus()
    rssi=net.getRssi()
    tm = misc.getClock()
    ret=
    {
        --核心板IMEI
        ID=bs.IMEI,
        --核心板SN
        SN=bs.SN,
        --核心板ID
        ID=bs.ID,
        --核心板MM
        MM=bs.MM,
        --网络状态
        NETSTATUS=create.getDatalink(),
        --基站定位成功
        isLocated=bs.isLocated,   
        --经度
        LONGITUDE=bs.LONGITUDE,
        --维度
        LATITUDE=bs.LATITUDE,      
        --设备端时间戳
        TIME=tm,
        --信号强度
        RSSI=rssi,
        --P13
        P13=P13Level    
    }
    dat=json.encode(ret)
    sys.publish("NET_HEART_MONI",dat)
end

--服务器连接成功后定时发送心跳数据包
sys.taskInit(function()
    while (NETSTATUS==false) do 
        sys.wait(1000)
    end
    while true do      
        ReportStatus()  
        --每10S发送一次心跳
        sys.wait(10000)
    end
end)

function GPIOChange(pin,level)
    log.info("upws","GPIOChange",level)
    if pin==13 then P13Level=level end
end

sys.subscribe("GPIO_LEVEL_CHANGE",GPIOChange)