
PROJECT = "Mi_Demo"
VERSION = "0.0.1"
AUTHOR = "Miuser"
PRODUCT_KEY = "eTRqO1hu3CpciZVfRdLfPPRu0VVarKRI "

--加载日志功能模块，并且设置日志输出等级
--如果关闭调用log模块接口输出的日志，等级设置为log.LOG_SILENT即可
require "log"
LOG_LEVEL = log.LOGLEVEL_TRACE
--[[
如果使用UART输出日志，打开这行注释的代码"--log.openTrace(true,1,115200)"即可，根据自己的需求修改此接口的参数
如果要彻底关闭脚本中的输出日志（包括调用log模块接口和Lua标准print接口输出的日志），执行log.openTrace(false,第二个参数跟调用openTrace接口打开日志的第二个参数相同)，例如：
1、没有调用过sys.opntrace配置日志输出端口或者最后一次是调用log.openTrace(true,nil,921600)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false)即可
2、最后一次是调用log.openTrace(true,1,115200)配置日志输出端口，此时要关闭输出日志，直接调用log.openTrace(false,1)即可
]]
--log.openTrace(true,1,115200)

require "sys"
require "net"
require "utils"
require "patch"
--每8S查询一次GSM信号强度
--每1分钟查询一次基站信息
net.startQueryAll(8000, 60000)

require "errDump"
errDump.request("udp://ota.airm2m.com:9072")
require "ntp"
ntp.timeSync(24, function()log.info(" AutoTimeSync is Done!") end)

--加载控制台调试功能模块（此处代码配置的是uart2，波特率115200）
--此功能模块不是必须的，根据项目需求决定是否加载
--使用时注意：控制台使用的uart不要和其他功能使用的uart冲突
--使用说明参考demo/console下的《console功能使用说明.docx》
--require "console"
--console.setup(2, 115200)

--加载错误日志管理功能模块【强烈建议打开此功能】
--如下2行代码，只是简单的演示如何使用errDump功能，详情参考errDump的api
require "errDump"
errDump.request("udp://ota.airm2m.com:9072")


--加载远程升级功能模块【强烈建议打开此功能，如果使用了阿里云的OTA功能，可以不打开此功能】
--如下3行代码，只是简单的演示如何使用update功能，详情参考update的api以及demo/update
--PRODUCT_KEY = "v32xEAKsGTIEQxtqgwCldp5aPlcnPs3K"
--require "update"
--update.request()


rtos.sleep(3000)
--启动并加载核心板适配包
require "bs"
--加载iRTU适配器
require "irtu_init"
--iRTU核心
require "default"
require "irtu_over"
--加载upws服务连接程序
require "upws"
--加载串口映射模块
require "com"
--加载OLED显示屏驱动
require "oled"
--加载双向IO驱动
require "bio"
--加载命令解释模块
require "cmd"
--加载调试模块
require "test"



--启动系统框架
sys.init(0, 0)
sys.run()
