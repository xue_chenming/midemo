--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com 
---------------------------------------------------------------------------------------------------------------------------------------

-- @模块功能：st7735显示屏简易驱动
-- @author miuser
-- @module midemo.st7735
-- @license MIT
-- @copyright miuser@luat
-- @release 2021-01-24
--------------------------------------------------------------------------
-- @使用方法
-- @7735DISPLAY,ABC ABC是您要输入的文字，支持中英文
-- @占用资源P0,P1,P2,P3

module(...,package.seeall)



--通过这个回调函数可以响应任意的串口或网络发布的命令
sys.subscribe("9A01DISPLAY",function(...)
    --通过arg可以从输入的命令行读入参数，并以逗号作为分隔符
    a=arg[1]
    b=arg[2]
    --通过write函数可以向串口和网络上报您的信息
    if (b==nil) then showText(a) else showText(a,b) end
    pub("9A01DISPLAY,"..a.." COMPLETED")
end)

local modulename=...
--注册到全局状态表
table.insert(status.INSTALLED,modulename)
--通过消息发送调试信息到串口和网络
function pub(s)
    s="["..(modulename).."]"..s
    sys.publish("COM",s.."\r\n")
    sys.publish("NET_CMD_MONI",s.."\r\n")
end

pub("P2 was occupied by DISPLAY_CK")
pub("P0 was occupied by DISPLAY_DA")
pub("P1 was occupied by DISPLAY_DC")
pub("P3 was occupied by DISPLAY_CS")

function showText(text,line)
    disp.clear()
    disp.setcolor(0x001F)
    --disp.puttext("---- MIDEMO ----",0,0)
    disp.setcolor(0x0000)
    disp.putimage("/lua/bg_round.png",0,0)
    local gbtext=common.utf8ToGb2312(text)
    if (line==nil) then
        --横向容纳的英文文字数，中文算两个字符
        local horizonletter=36
        local linecount=(#gbtext/horizonletter)+1
        for i=1,linecount do
            local linestr=string.sub(gbtext,(i-1)*horizonletter+1,(i-1)*horizonletter+horizonletter)
            log.info("gc9a01","linstr="..linestr)
            disp.puttext(linestr,2,horizonletter+i*horizonletter)
        end        
        --disp.puttext(gbtext,2,32)
    else
        disp.puttext(gbtext,2,(line+1)*16)
    end
    disp.update()
end


local function init()
    local para =
    {
        width = 240, --分辨率宽度，240像素；用户根据屏的参数自行修改
        height = 240, --分辨率高度，240像素；用户根据屏的参数自行修改
        bpp = 16, --位深度，彩屏仅支持16位
        bus = disp.BUS_SPI4LINE, --LCD专用SPI引脚接口，不可修改
        xoffset = 0, --X轴偏移
        yoffset = 0, --Y轴偏移
        freq = 13000000, --spi时钟频率，支持110K到13M（即110000到13000000）之间的整数（包含110000和13000000）
        hwfillcolor = 0xFFFFFF, --填充色，黑色
        pinrst = pio.P0_6, --reset，复位引脚
        pinrs = pio.P0_1, --rs，命令/数据选择引脚
        --初始化命令
        --前两个字节表示类型：0001表示延时，0000或者0002表示命令，0003表示数据
        --延时类型：后两个字节表示延时时间（单位毫秒）
        --命令类型：后两个字节命令的值
        --数据类型：后两个字节数据的值
        initcmd =
        {
            0x000200EF,
            0x000200EB,
            0x00030014,

            0x000200FE,
            0x000200EF,

            0x000200EB,
            0x00030014,

            0x00020084,
            0x00030040,

            0x00020085,
            0x000300FF,

            0x00020086,
            0x000300FF,

            0x00020087,
            0x000300FF,

            0x00020088,
            0x0003000A,

            0x00020089,
            0x00030021,

            0x0002008A,
            0x00030000,

            0x0002008B,
            0x00030080,

            0x0002008C,
            0x00030001,

            0x0002008D,
            0x00030001,

            0x0002008E,
            0x000300FF,

            0x0002008F,
            0x000300FF,

            0x000200B6,
            0x00030000,
            0x00030020,

            0x00020036,
            0x00030008,
            --0x000300C8,
            --0x00030068,
            --0x000300A8,

            0x0002003A,
            0x00030005,

            0x00020090,
            0x00030008,
            0x00030008,
            0x00030008,
            0x00030008,

            0x000200BD,
            0x00030006,

            0x000200BC,
            0x00030000,

            0x000200FF,
            0x00030060,
            0x00030001,
            0x00030004,

            0x000200C3,
            0x00030013,
            0x000200C4,
            0x00030013,

            0x000200C9,
            0x00030022,

            0x000200BE,
            0x00030011,

            0x000200E1,
            0x00030010,
            0x0003000E,

            0x000200DF,
            0x00030021,
            0x0003000C,
            0x00030002,

            0x000200F0,
            0x00030045,
            0x00030009,
            0x00030008,
            0x00030008,
            0x00030026,
            0x0003002A,

            0x000200F1,
            0x00030043,
            0x00030070,
            0x00030072,
            0x00030036,
            0x00030037,
            0x0003006F,

            0x000200F2,
            0x00030045,
            0x00030009,
            0x00030008,
            0x00030008,
            0x00030026,
            0x0003002A,

            0x000200F3,
            0x00030043,
            0x00030070,
            0x00030072,
            0x00030036,
            0x00030037,
            0x0003006F,

            0x000200ED,
            0x0003001B,
            0x0003000B,

            0x000200AE,
            0x00030077,

            0x000200CD,
            0x00030063,

            0x00020070,
            0x00030007,
            0x00030007,
            0x00030004,
            0x0003000E,
            0x0003000F,
            0x00030009,
            0x00030007,
            0x00030008,
            0x00030003,

            0x000200E8,
            0x00030034,

            0x00020062,
            0x00030018,
            0x0003000D,
            0x00030071,
            0x000300ED,
            0x00030070,
            0x00030070,
            0x00030018,
            0x0003000F,
            0x00030071,
            0x000300EF,
            0x00030070,
            0x00030070,

            0x00020063,
            0x00030018,
            0x00030011,
            0x00030071,
            0x000300F1,
            0x00030070,
            0x00030070,
            0x00030018,
            0x00030013,
            0x00030071,
            0x000300F3,
            0x00030070,
            0x00030070,

            0x00020064,
            0x00030028,
            0x00030029,
            0x000300F1,
            0x00030001,
            0x000300F1,
            0x00030000,
            0x00030007,

            0x00020066,
            0x0003003C,
            0x00030000,
            0x000300CD,
            0x00030067,
            0x00030045,
            0x00030045,
            0x00030010,
            0x00030000,
            0x00030000,
            0x00030000,

            0x00020067,
            0x00030000,
            0x0003003C,
            0x00030000,
            0x00030000,
            0x00030000,
            0x00030001,
            0x00030054,
            0x00030010,
            0x00030032,
            0x00030098,

            0x00020074,
            0x00030010,
            0x00030085,
            0x00030080,
            0x00030000,
            0x00030000,
            0x0003004E,
            0x00030000,

            0x00020098,
            0x0003003E,
            0x00030007,

            0x00020035,
            0x00020021,

            0x00020011,
            0x00010060,
            0x00010060,
            0x00020029,
            0x00010020,
        },
        --休眠命令
        sleepcmd = {
            0x00020010,
        },
        --唤醒命令
        wakecmd = {
            0x00020011,
        }
    }
    disp.init(para)
    disp.clear()
    disp.update()
end

--初始化
init()
--显示标题
--showText("UART DISPLAY")