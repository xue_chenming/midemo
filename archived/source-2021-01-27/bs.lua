--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭、陈夏等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)、LLCOM
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com，如有侵权嫌疑将立即纠正
---------------------------------------------------------------------------------------------------------------------------------------
--- 模块功能：核心板适配文件，适配硬件版本Cat1 Phone Core V2b
-- @author miuser
-- @module midemo.bs
-- @license MIT
-- @copyright miuser@luat
-- @release 2021-01-27
--------------------------------------------------------------------------
require "lbsLoc"
require "misc"
module(...,package.seeall)

--------------------核心板状态全局变量-------------------------------
--isLocated：number类型，0表示成功，1表示网络环境尚未就绪，2表示连接服务器失败，3表示发送数据失败，4表示接收服务器应答超时，5表示服务器返回查询失败；为0时，后面的3个参数才有意义
--LATITUDE：string类型，纬度，整数部分3位，小数部分7位，例如031.2425864
--LONGITUDE：string类型，经度，整数部分3位，小数部分7位，例如121.4736522
LONGITUDE=0
LATITUDE=0
isLocated=0
--以下信息开机后从模块内部获取，模块之间不重复
--模块ID
ID=""
--模块密码
MM=""
--模块出厂序列号
SN=""
--模块IMEI号
IMEI=""

--等待系统初始化完成后加载，此处不要删除
sys.timerStart(function()
--------------------------------------------------------------------
--                                                                 -
--                           核心模块加载表                        -
--                                                                 -
--------------------------------------------------------------------
-- 加载帮助模块
require "help"
--加载串口映射模块
require "com"
--加载iRTU适配器
require "irtu_init"
--iRTU核心
require "default"
--加载后清理工作
require "irtu_over"
--加载upws服务连接程序
require "upws"
--加载阿里云服务连接程序
require "maliyun"
--加载mqtt服务连接程序
require "mmqtt"
--根据bs设置，决定是否加载OLED显示屏驱动
require "oled"
--加载IO输出驱动
require "gpo"
--加载双向IO驱动
require "bio"
--加载命令解释模块
require "cmd"
--加载电源按键模块
require "mkey"
--加载打电话模块
require "onedial"
--打补丁后的语音朗读模块（占用100kflash）
require "ttsplus"
--原生语音朗读模块，与 TTSPLUS二选一加载即可
--require"tts"
--短信收发
require "trsms"
--根据bs设置，决定是否加载WS2812Matrix显示屏驱动
require "ws2812bmatrix" 
--根据bs设置，决定是否加载Hyperstepper开源电机驱动
--require "hyperstepper"
--加载GPS模块
--require "mgps"
--加载按钮模块
require "btn"
--加载st7735显示屏
require "st7735"
--------------------------------------------------------------------
--                                                                 -
--                           三方模块加载表                        -
--                                                                 -
--------------------------------------------------------------------
--SHT30读取模块
require "sht30"
--外部模块DEMO
require "demo"

log.info("midemo","midemo started")

--此处不要删除
end , 3000)


--------------------------------------------------------------
---------以下内容为内部函数，不需要用户修改--------------------
--------------------------------------------------------------



--基站定位
local function reqLbsLoc()   
    lbsLoc.request(getLocCb)
end

--获取基站对应的经纬度后的回调函数
function getLocCb(result,lat,lng)
    log.info("testLbsLoc.getLocCb",result,lat,lng)
    isLocated=result
    LATITUDE=lat
    LONGITUDE=lng

    --获取经纬度成功
    if result==0 then
    --失败
    else
    end
    sys.timerStart(reqLbsLoc,20000)
end
reqLbsLoc()

--IMEI和SN查询
local function rsp(cmd, success, response, intermediate)
   local prefix = string.match(cmd, "AT(%+%u+)")
   --查询序列号
   if cmd == "AT+WISN?" then
       result = (intermediate~="*CME ERROR: Missing SN")
       if result then
           bs.SN = intermediate 
           bs.MM="00"..SN
           log.info("bs","SN="..intermediate)
       end
       if setSnCbFnc then setSnCbFnc(result) end  
   --查询IMEI
   elseif cmd == "AT+CGSN" then
       bs.IMEI= intermediate
       bs.ID=string.sub(IMEI,-10)
       log.info("bs","IMEI="..intermediate)
   end
end

--查询序列号
ril.request("AT+WISN?")
--查询IMEI
ril.request("AT+CGSN")
--注册以下AT命令的应答处理函数
ril.regRsp("+WISN", rsp)
ril.regRsp("+CGSN", rsp)


VLCD=midemo.VLCD or 16
VMMC=midemo.VMMC or 2

--电压域设定
pmd.ldoset(VLCD,pmd.LDO_VLCD)
pmd.ldoset(VMMC,pmd.LDO_VMMC)



