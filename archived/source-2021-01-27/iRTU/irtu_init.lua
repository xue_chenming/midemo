--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com 
---------------------------------------------------------------------------------------------------------------------------------------

-- @模块功能：iRTU固件执行前适配
-- @author miuser
-- @module midemo.irtu_init
-- @license MIT
-- @copyright miuser@luat
-- @release 2020-09-08
--------------------------------------------------------------------------
------------------------iRTU配置信息---------------------------------
--通过修改当前目录的irtu.json，可以对iRTU模块初始状态进行配置
--通过create.getDatalink()函数获取iRTU服务器连接状态 true为已经连接
--通过sys.publish("NET_SENT_RDY_1",str)向服务器发送数据
--通过 sys.subscribe("NET_RECV_WAIT_1", function(msg) ...end) 接收服务器发送来的数据

--------------------------------------------------------------------------

require "bs"
--以下部分的代码无须用户修改
module(..., package.seeall)
--_G.PRODUCT_KEY = "DPVrZXiffhEUBeHOUwOKTlESam3aXvnR"
--iRTU的缺省配置文件，描述了首次烧写后的iRTU行为
CONFIG="irtuupws.json"


local function readfile(filename)--打开指定文件并输出内容
    
    local filehandle=io.open(filename,"r")--第一个参数是文件名，第二个是打开方式，'r'读模式,'w'写模式，对数据进行覆盖,'a'附加模式,'b'加在模式后面表示以二进制形式打开
    if filehandle then          --判断文件是否存在
        local fileval=filehandle:read("*all")--读出文件内容
      if  fileval  then
           print(fileval)  --如果文件存在，打印文件内容
           filehandle:close()--关闭文件
           return fileval
      else 
           print("The file is empty")--文件不存在
      end
    else 
        print("文件不存在或文件输入格式不正确") --打开失败  
    end    
end


local function writevalw(filename,value)--在指定文件中添加内容
    local filehandle = io.open(filename,"w")--第一个参数是文件名，后一个是打开模式'r'读模式,'w'写模式，对数据进行覆盖,'a'附加模式,'b'加在模式后面表示以二进制形式打开
    if filehandle then
        filehandle:write(value)--写入要写入的内容
        filehandle:close()
    else
        log.info("midemo.irtu_adapter","irtu适配文件不存在或文件输入格式不正确") 
    end
end
CONFIG="/lua/"..CONFIG

content=readfile(CONFIG)
config=json.decode(content)

config["conf"][1]="udp"
config["conf"][2]=""
config["conf"][3]="0"
config["conf"][4]="box.miuser.net"
config["conf"][5]="7101"
config["conf"][6]="7101"

content_modified=json.encode(config)
log.info("updated json",content)
writevalw("/CONFIG.cnf",content)

