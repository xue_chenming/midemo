--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com 
---------------------------------------------------------------------------------------------------------------------------------------

-- @模块功能：帮助模块
-- @author 作者
-- @module midemo.help
-- @license MIT
-- @copyright miuser@luat
-- @release 2020-09-29
--------------------------------------------------------------------------
-- @本模块用来呈现 help.md中的内容

module(...,package.seeall)


--通过这个回调函数可以响应任意的串口或网络发布的命令
sys.subscribe("HELP",function(...)
    --通过arg可以从输入的命令行读入参数，并以逗号作为分隔符
    --a=arg[1]
    --b=arg[2]
    --c=arg[3]
    --通过pub函数可以向串口和网络上报您的信息
    doc=readfile("/lua/help.md")
    pub(doc)
end)


local modulename=...
--注册到全局状态表
table.insert(status.INSTALLED,modulename)
--通过消息发送调试信息到串口和网络
function pub(s)
    s="["..(modulename).."]"..s
    sys.publish("COM",s.."\r\n")
    sys.publish("NET_CMD_MONI",s.."\r\n")
end



function readfile(filename)--打开指定文件并输出内容
    
    local filehandle=io.open(filename,"r")--第一个参数是文件名，第二个是打开方式，'r'读模式,'w'写模式，对数据进行覆盖,'a'附加模式,'b'加在模式后面表示以二进制形式打开
    if filehandle then          --判断文件是否存在
        local fileval=filehandle:read("*all")--读出文件内容
        if  fileval  then
           filehandle:close()--关闭文件
           table=split(fileval)
           log.info("help",unpack(table))
           return fileval  --如果文件存在，打印文件内容
        else 
           print("help file not found")--文件不存在
        end
    else 
        print("文件不存在或文件输入格式不正确") --打开失败  
    end 
    
end

function split( str )
    local resultStrList = {}
    string.gsub(str,',',function ( w )
        table.insert(resultStrList,w)
    end)
    return resultStrList
end
