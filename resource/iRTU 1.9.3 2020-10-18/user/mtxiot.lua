--------------------------------------------------------------------------------------------------------------------------------------- 
--版权声明：本demo集源于上海合宙官方技术团队的DEMO（MIT），并参考了合宙开源社区的众多大佬无私分享的代码，包括但不限于 稀饭放姜、Wendal、晨旭等
-- 目前参考到的开源项目有： iRTU（MIT)、LuatOS(MIT)
-- 欲获取更多luat 代码请访问 doc.openluat.com
-- 如果您觉得本demo集包含了未经由您授权的代码，请联系 64034373@qq.com 
---------------------------------------------------------------------------------------------------------------------------------------

-- @模块功能：腾讯物联网通信云透传
-- @author miuser
-- @module midemo.txiot
-- @license MIT
-- @copyright miuser@luat
-- @release 2020-10-29
--------------------------------------------------------------------------
-- @使用方法
-- @通过irtuupws.json配置，将2-8中的任意通道配置，配置为腾讯云物联网服务器连接
-- 订阅服务端主题名为 91JGKYBZZD/${deviceName}/control，其中91JGKYBZZD需要替换成您的产品ID(ProductKey)
-- 设备端发布的主题名为91JGKYBZZD/${deviceName}/event
-- @通过bs.lua配置，将前面配置的腾讯云通道映射到硬件串口上 TXIOT_COM=3
-- @通过订阅 名为“TXIOT_CONTROL”的系统消息接收腾讯云服务器下发的数据
-- @通过发布 名为“TXIOT_EVENT"的系统消息发送数据到腾讯云服务器

require "net"
require "misc"
require "common"
module(...,package.seeall)


--自定义心跳包
function HeartBeat()
    rssi=net.getRssi()
    tm = misc.getClock()
    ret=
    {
        --核心板IMEI
        ID=bs.IMEI,
        --核心板SN
        SN=bs.SN,
        --核心板ID
        ID=bs.ID,
        --核心板MM
        MM=bs.MM,
        --网络状态
        NETSTATUS=create.getDatalink(),
        --基站定位成功
        isLocated=bs.isLocated,   
        --经度
        LONGITUDE=bs.LONGITUDE,
        --维度
        LATITUDE=bs.LATITUDE,      
        --设备端时间戳
        TIME=tm,
        --信号强度
        RSSI=rssi,
    }
    dat=json.encode(ret)
    sys.publish("TXIOT_EVENT",dat)
end

--服务器连接成功后定时发送心跳数据包
sys.taskInit(function()
    while (NETSTATUS==false) do 
        sys.wait(1000)
    end
    while true do      
        HeartBeat()  
        --每10S发送一次心跳
        sys.wait(10000)
    end
end)


--接受irtu收到的网络数据包进行处理
sys.subscribe("UART_SENT_RDY_"..bs.TXIOT_CHANNEL,function(uid,msg)
    log.info("midemo.txiot","received:"..msg)
    sys.publish("TXIOT_CONTROL",msg)
end)

--发送本地数据包到iRTU转发
sys.subscribe("TXIOT_EVENT", function(str)
    log.info("midemo.txiot","sent:"..str)
    sys.publish("NET_SENT_RDY_"..bs.TXIOT_CHANNEL,str)
end)

-- 串口ID,串口读缓冲区
local sendQueue= {}
-- 串口超时，串口准备好后发布的消息
-- 这个参数要根据波特率调整，波特率如果比较低，相应的timeout要延长，单位是ms
-- 一般来说115200bps建议用25, 9600bps建议调到100
local uartimeout= 25
--保持系统处于唤醒状态，不会休眠
pm.wake("com")
-- 初始化所有被指派为腾讯云透传的串口
for i=1,#bs.TXIOT_COM do
    uart.setup(bs.TXIOT_COM[i], 115200, 8, uart.PAR_NONE, uart.STOP_1, nil, 1)
    uart.on(bs.TXIOT_COM[i], "receive", function(uid)
        table.insert(sendQueue, uart.read(uid, 8192))
        sys.timerStart(sys.publish, uartimeout, "TXIOTCOMRSV")
    end)
end

-- 2 将串口收到的消息转发到腾讯云端口
sys.subscribe("TXIOTCOMRSV", function()
    local str = table.concat(sendQueue)
    str=common.gb2312ToUtf8(str)
    sys.publish("TXIOT_EVENT",str)
    sendQueue={}
end)

-- 向所有腾讯云串口发送字符串
function write(str)
    for i=1,#bs.TXIOT_COM do
        uart.write(bs.TXIOT_COM[i], common.utf8ToGb2312(str))
    end
end

--从系统消息接收主题为“TXIOT_CONTROL”的消息，并转发到串口
local function uartrsv(msg)
    for i=1,#bs.TXIOT_COM do
        uart.write(bs.TXIOT_COM[i], common.utf8ToGb2312(msg))
    end
end
sys.subscribe("TXIOT_CONTROL", uartrsv)